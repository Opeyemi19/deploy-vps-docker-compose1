#!/bin/sh

echo WEB_IMAGE=$IMAGE:web1  >> .env
echo NGINX_IMAGE=$IMAGE:nginx1  >> .env
echo CI_REGISTRY_USER=$CI_REGISTRY_USER   >> .env
echo CI_BUILD_TOKEN=$CI_BUILD_TOKEN  >> .env
echo CI_REGISTRY=$CI_REGISTRY  >> .env
echo IMAGE=$CI_REGISTRY_IMAGE >> .env